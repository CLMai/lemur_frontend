﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Hub.aspx.cs" Inherits="LemurID.Interns.Hub" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>The Intern Hub</title>
    <meta charset="UTF-8" />

    <!-- Javascript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

    <!-- Compiled and minified UA Bootstrap CSS, icon font not included -->
    <link rel="stylesheet" href="https://cdn.uadigital.arizona.edu/lib/ua-bootstrap/v1.0.0-beta.26/ua-bootstrap.min.css" />

    <%--Normal BootStrap--%>
    <link href="../css/bootstrap.min.css" rel="stylesheet" />

    <%--Font Awesome--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" />


    <!-- Milo -->
    <link rel="stylesheet" href="https://brand.arizona.edu/sites/default/files/ua-banner/ua-web-branding/ua-fonts/milo.css">

    <%--Other CSS--%>
    <link href="Hub.css" rel="stylesheet" />

</head>
<body>
    <div id="topContainer" class="container">
        <div class="col-md-5" style="margin-top: 1vh;">
            <div id="internGreeting" style="">
                <h1 style="border-radius: 10px;">The Intern Hub</h1>
                <h4>Welcome
                        <asp:Label ID="userhandle" runat="server">Guest</asp:Label><span id="userid" runat="server"></span></h4>
                <div class="row">
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-5" style="margin-top: 3vh;">
            <div id="techCoreLogo" style="text-align: right;">
                <a href="/" class="header_logo" rel="home" id="logo" style="height: 60px;">
                    <img id="techCoreLogoInternHub" src="https://techcoreassets.blob.core.windows.net/images/techcore_blacktext.png" alt="Tech Core | Home" class="header-logo-image-2" style="" />
                </a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row" id="internHubNavRow">
            <div class="col-md-2 internHubNavBarOptionDiv">
                <div class="clickableElement" style="cursor: pointer;" onclick="showInternTeamsDiv()">
                    <div class="internHubNavBarOption">
                        <h4><i class="fas fa-users" style="padding-right: 10px;"></i>Teams</h4>
                    </div>
                </div>
            </div>
        </div>
        <div id="internHubDisplayContainer" class="row">

            <%--------------------------------------%>
            <%-- Coders --%>
            <%--------------------------------------%>


            <div id="internTeamsDiv" style="border-radius: 10px;">
                <div style="padding: 10px;">
                    <div id="ncbtn" runat="server">
                        <button id="newintern" type="button" class="btn btn-danger" data-toggle="modal" data-target="#addnewintern">Add New Coder</button>
                        <%--<button id="coderBadge" type="button" class="btn btn-danger" style="padding: 10px;">Add Badge To Intern</button>--%>
                    </div>
                    <div id="InternsTable">
                    </div>
                </div>
            </div>

        </div>
    </div>

    <%--------------------------------------%>
    <%-- Modals --%>
    <%--------------------------------------%>

    <%--INTERN MODALS--%>



    <%-------------------------------------------------------------%>
    <%--------------------------ADD INTERN-------------------------%>
    <%-------------------------------------------------------------%>
    <div class="modal" tabindex="-1" role="dialog" id="addnewintern">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Coder</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="newcoderbody">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-mid-6">

                            </div>
                        </div>
                    </div>
                    <span>UserId: </span>
                    <input id="newinternuserid" value="" type="text" runat="server" /><br />
                    <span>First Name: </span>
                    <input id="newinternfirst" value="" type="text" runat="server" /><br />
                    <span>Last Name: </span>
                    <input id="newinternlast" value="" type="text" runat="server" /><br />
                    <br />
                    <div id="adminToggles" class="adminToggles" runat="server">
                        <span>Team:</span>
                        <select id="newinternteam" name="addInternTeam">
                            <option value="Data">Data</option>
                            <option value="Operations">Operations</option>
                            <option value="Tech">Tech</option>
                            <option value="Creative">Creative</option>
                        </select>
                        <br />
                        <span>Role:</span>
                        <select id="addinternRole" name="addinternRole">
                            <option value="Coder">Coder</option>
                            <option value="Admin">Admin</option>
                            <option value="Intern">Intern</option>
                        </select>
                        <br />
                        <span>Status:</span>
                        <select id="addinternStatus" name="addinternStatus" runat="server">
                            <option value="Active">Active</option>
                            <option value="Retired">Retired</option>
                            <option value="New Hire">New Hire</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submitNewIntern()" runat="server">Submit New Intern</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <%-------------------------------------------------------------%>
    <%---------------------------EDIT INTERN-----------------------%>
    <%-------------------------------------------------------------%>
    <form runat="server">
        <div class="modal fade" tabindex="-1" role="dialog" id="editInterns">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Intern</h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body editinternModalBody">
                        <div class="container-fluid">
                            <input id="editInternId" type="text" runat="server" style="display: none;" />
                            <span class="editmodalInternHubHeader">Intern:</span>
                            <input type="text" id="editInternUserId" style="border: none !important; font-size: large;" class="" runat="server" readonly />
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="editmodalInternHubHeader">First Name:</span>
                                    <input type="text" id="editInternFirst" class="editmodalInternHub" runat="server" />
                                </div>
                                <div class="col-md-6">
                                    <span class="editmodalInternHubHeader">Last Name:</span>
                                    <input type="text" id="editInternLast" class="editmodalInternHub" runat="server" />
                                </div>
                            </div>
                            <div class="row adminToggles">
                                <div class="col-md-4">
                                    <span class="editmodalInternHubHeader">Role:</span>
                                    <select id="editInternRole" class="editmodalInternHub" name="editInternRole" runat="server">
                                        <option value="Coder">Coder</option>
                                        <option value="Admin">Admin</option>
                                        <option value="Intern">Intern</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <span class="editmodalInternHubHeader">Status:</span>
                                    <select id="editInternStatus" class="editmodalInternHub" name="editInternStatus" runat="server">
                                        <option value="Active">Active</option>
                                        <option value="Retired">Retired</option>
                                        <option value="New Hire">New Hire</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <span class="editmodalInternHubHeader">Team:</span>
                                    <select id="editInternTeam" class="editmodalInternHub" name="editInternTeam" runat="server">
                                        <option value="data">Data</option>
                                        <option value="operations">Operations</option>
                                        <option value="creative">Creative</option>
                                        <option value="tech">Tech</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onserverclick="submitEditIntern" runat="server">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- jQuery 3.2.1 -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.cookie.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>

    <%-- SWEET ALERTS --%>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.0.3/dist/sweetalert2.all.min.js"></script>

    <script src="Hub.js"></script>
</body>
</html>
