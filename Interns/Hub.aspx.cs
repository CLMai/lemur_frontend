﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LemurID.Interns
{
    public partial class Hub : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie TGCookie = new HttpCookie("IH");
            TGCookie = Request.Cookies["IH"];
            // Check to see if the cookie is empty
            if (TGCookie != null) // Yes, let them in!
            {
                string Token = TGCookie["Token"].ToString();
                string user = Get_UserHandle(Token);
                userhandle.Text = user;
                if (CheckAdmin(Token))
                {
                    adminToggles.Style.Add("display", "inline");
                    ncbtn.Style.Add("display", "inline");
                }
            }

        }

        public string Get_UserHandle(string Token)
        {
            string UserHandle = "Guest";
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("SELECT A.ID, A.FirstName, A.LastName, A.UserID FROM Lemur_Users AS A JOIN Lemur_UserSessions AS B ON A.UserId = B.UserId WHERE B.Token = @Token", Conn);
                Com.Parameters.Add(new SqlParameter("Token", Token));
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    userid.InnerText = r[0].ToString();
                    UserHandle = r[1].ToString() + " " + r[2].ToString() + " (" + r[3].ToString() + ")";
                }
            }
            return UserHandle;
        }

        public bool CheckAdmin(string Token)
        {
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("Lemur_SELECT_CheckAdmin", Conn);
                Com.Parameters.AddWithValue("Token", Token);
                Com.CommandType = CommandType.StoredProcedure;
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    string role = r[0].ToString();
                    if (role == "Admin")
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        protected void submitEditIntern(object sender, EventArgs e)
        {
            string id = editInternId.Value;
            string userid = editInternUserId.Value;
            string first = editInternFirst.Value;
            string last = editInternLast.Value;
            string role = editInternRole.Value;
            string status = editInternStatus.Value;
            string year = DateTime.Now.Year.ToString();
            string team = editInternTeam.Value;
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("Lemur_UPDATE_EditUser", Conn);
                Com.Parameters.AddWithValue("id", id);
                Com.Parameters.AddWithValue("userid", userid);
                Com.Parameters.AddWithValue("first", first);
                Com.Parameters.AddWithValue("last", last);
                Com.Parameters.AddWithValue("role", role);
                Com.Parameters.AddWithValue("status", status);
                Com.Parameters.AddWithValue("team", team);
                Com.CommandType = CommandType.StoredProcedure;
                Com.ExecuteNonQuery();
            }
        }
    }
}