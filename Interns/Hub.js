$(document).ready(function () {
    showInternTeamsDiv()
    
});


function populateInternsTable() {
    let token = $.cookie("IH");
    let userID = document.getElementById('userhandle');
    //alert(userID);
    userID = userID.innerHTML.split("(")[1];
    //console.log(userID);
    userID = userID.slice(0, userID.length - 1);
    //console.log(userID)
    $.ajax({
        type: 'GET',
        url: '/api/Lemur/populateInternsTable?' + token + '&coderID=' + userID,
        cache: false,
        success: function (result) {
            //console.log(result)
            document.getElementById("InternsTable").innerHTML = result;
        }
    });
}

function submitNewIntern() {
    var internuserid = $('#newinternuserid').val();
    var internfirst = $('#newinternfirst').val();
    var internlast = $('#newinternlast').val();
    var internrole = $('#addinternRole').val();
    var internstatus = $('#addinternStatus').val();
    var internteam = $('#newinternteam').val();
    //console.log(interncolor);
    $.ajax({
        type: 'POST',
        url: '/api/Lemur/SubmitNewIntern?internuserid=' + internuserid + '&internfirst=' + internfirst + '&internlast=' + internlast + '&internrole=' + internrole + '&internstatus=' + internstatus + '&internteam=' + internteam,
        cache: false,
        success: function (result) {
            $('#addnewintern').modal('hide');
            populateInternsTable();
        }
    });
}

function editCoder(button) {
    let userhandle = document.getElementById('userhandle');
    //alert(userID);
    userhandle = userhandle.innerHTML.split("(")[1];
    //console.log(userID);
    userhandle = userhandle.slice(0, userhandle.length - 1);
    //alert(userID);
    var token = $.cookie("IH");
    var modal = $('#editInterns');
    var body = modal.find('.modal-body');
    var id = $(button).closest('tr').children().first().text().trim();
    var userid = $(button).closest('tr').children().eq(1).text().trim();
    var first = $(button).closest('tr').children().eq(2).text().trim();
    var last = $(button).closest('tr').children().eq(3).text().trim(); 
    var role = $(button).closest('tr').children().eq(4).text().trim();
    var status = $(button).closest('tr').children().eq(5).text().trim();
    var team = $(button).closest('tr').children().eq(6).text().trim();
    //console.log(team);


    $.ajax({
        type: 'get',
        url: '/api/Lemur/CheckAdmin?' + token,
        cache: false,
        success: function (result) {
            //console.log(result)
            //console.log(userid)
            //console.log(userhandle)
            if (result == true || userid == userhandle) {
                if (result == true) body.find('.adminToggles').show();
                else body.find('.adminToggles').hide();
                body.find('#editInternTeam').val(team);
                body.find('#editInternRole').val(role);
                body.find('#editInternId').val(id);
                body.find('#editInternStatus').val(status);
                body.find('#editInternUserId').val(userid);
                body.find('#editInternFirst').val(first);
                body.find('#editInternLast').val(last);
                modal.modal('show');
            } else {
                body.find('.adminToggles').css('display', 'none');
                Swal.fire({
                    title: 'Unworthy',
                    text: "",
                });
            }
        }
    });

}

function Delete(idtable) {
    let token = $.cookie("IH");
    var table = "";
    var ID = parseInt(idtable);
    $.ajax({
        type: 'get',
        url: '/api/Lemur/CheckAdmin?' + token,
        cache: false,
        success: function (result) {
            if (result == true) {
                //alert('broken')
                //console.log(idtable)
                if (idtable.includes("sponsor")) {
                    table = "sponsor";
                }
                else if (idtable.includes("project")) {
                    table = "project";
                }
                else if (idtable.includes("intern")) {
                    table = "intern";
                }
                Swal.fire({
                    title: 'Are you sure you want to delete this ' + table + '?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '/api/Lemur/DeleteEntry?table=' + table + "&ID=" + ID,
                            cache: false,
                            success: function (result) {
                                if (table === 'project') {
                                    showProjects();
                                }
                                else if (table === 'sponsor') {
                                    showSponsors();
                                }
                                else if (table === 'intern') {
                                    populateInternsTable();
                                }
                                Swal.fire(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success'
                                );

                            }
                        });
                    }
                });
            } else {
                Swal.fire({
                    title: 'Unworthy',
                    text: "This option is only accessable by Admins, Peasant",
                });
            }
        }
    });
}


function showInternTeamsDiv() {
    hideAllDivs();
    $("#internScheduleDiv").hide();
    populateInternsTable();
    $("#internTeamsDiv").show();

};

function hideAllDivs() {
    $("#internTeamsDiv").hide();
}
