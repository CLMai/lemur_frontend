﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace LemurID.Controllers
{
    public class LemurController : ApiController
    {

        [HttpGet]
        public string testing()
        {
            return "this is a test";
        }

        [HttpGet]
        public bool CheckAdmin(string Token)
        {
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("Lemur_SELECT_CheckAdmin", Conn);
                Com.Parameters.AddWithValue("Token", Token);
                Com.CommandType = CommandType.StoredProcedure;
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    string role = r[0].ToString();
                    if (role == "Admin")
                    {
                        return true;
                    }
                }
            }
            return false;
        }




        [HttpPost]
        public void SubmitNewIntern(string internuserid, string internfirst, string internlast, string internrole, string internstatus, string internteam)
        {
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("Lemur_INSERT_AddUser", Conn);
                Com.Parameters.AddWithValue("userid", internuserid);
                Com.Parameters.AddWithValue("first", internfirst);
                Com.Parameters.AddWithValue("last", internlast);
                Com.Parameters.AddWithValue("role", internrole);
                Com.Parameters.AddWithValue("status", internstatus);
                Com.Parameters.AddWithValue("team", internteam);
                Com.CommandType = CommandType.StoredProcedure;
                Com.ExecuteNonQuery();
            }
        }


        [HttpPost]
        public void DeleteEntry(string table, int ID)
        {
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("Lemur_DELETE_DeleteEntry", Conn);
                Com.Parameters.AddWithValue("id", ID);
                Com.Parameters.AddWithValue("table", table);
                Com.CommandType = CommandType.StoredProcedure;
                Com.ExecuteNonQuery();
            }
        }



        [HttpGet]
        public string populateInternsTable(string token, string coderID)
        {
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {

                string Inner = "";
                string DataTeam = "<table id = \"DataTeamTable\" class=\"table table-bordered\"><div><h3>Team Data</h3></div><thead><tr><th style = \"display: none\" > ID </th><th><span id=\"coderid\">Intern</span></th><th><span id = \"coderfirst\"> First Name</span> </th><th><span id = \"coderlast\" > Last Name</span> </th></tr></thead><tbody id = \"coderbody\" >";
                string CreativeTeam = "<table id = \"CreativeTeamTable\" class=\"table table-bordered\"><div><h3>Team Creative</h3></div><thead><tr><th style = \"display: none\"> ID </th><th><span id=\"coderid\">Intern</span></th><th><span id = \"coderfirst\" > First Name</span> </th><th><span id = \"coderlast\" > Last Name</span> </th></tr></thead><tbody id = \"coderbody\" >";
                string TechTeam = "<table id = \"TechTeamTable\" class=\"table table-bordered\"><div><h3>Team Tech</h3></div><thead><tr><th style = \"display: none\"> ID </th><th><span id=\"coderid\">Intern</span></th><th><span id = \"coderfirst\" > First Name</span> </th><th><span id = \"coderlast\" > Last Name</span> </th></tr></thead><tbody id = \"coderbody\" >";
                string OperationsTeam = "<table id = \"OperationsTeamTable\" class=\"table table-bordered\"><div><h3>Team Operations</h3></div><thead><tr><th style = \"display: none\">ID</th><th><span id=\"coderid\">Intern</span></th><th><span id = \"coderfirst\" > First Name</span> </th><th><span id = \"coderlast\" > Last Name</span> </th></tr></thead><tbody id = \"coderbody\" >";

                Conn.Open();
                SqlCommand Com = new SqlCommand("Lemur_SELECT_PopulateUsersTable", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                SqlDataReader r = Com.ExecuteReader();
                int rowNum = 0;
                while (r.Read())
                {

                    string id = r[0].ToString();
                    string userid = r[1].ToString();
                    string first = r[2].ToString();
                    string last = r[3].ToString();
                    string role = r[4].ToString();
                    string status = r[5].ToString();
                    string team = r[6].ToString().ToLower();

                    if (team == "data")
                    {
                        DataTeam += "<tr class = \"SELECTME\" value=\"1\" id = \"row " + rowNum + "\"" + ">";
                        DataTeam += "<th style=\"display:none\">" + id + "</th>";
                        DataTeam += "<th> " + userid + "</th>";
                        DataTeam += "<th> " + first + "</th>";
                        DataTeam += "<th> " + last + "</th>";
                        DataTeam += "<th style=\"display:none\"> " + role + "</th>";
                        DataTeam += "<th style=\"display:none\"> " + status + "</th>";
                        DataTeam += "<th style=\"display: none;\"> " + team + "</th>";
                        if (CheckAdmin(token) || userid == coderID)
                        {
                            DataTeam += "<th class=\"edit\"><button class = \"edit\" onclick =\"editCoder(this)\">Edit</button></th>";
                            DataTeam += "<th class=\"delete\"><button class = \"delete\" onclick =\"Delete('" + id + "intern');\">Delete</button></th>";
                        }
                    }
                    else if (team == "creative")
                    {
                        CreativeTeam += "<tr class = \"SELECTME\" value=\"1\" id = \"row " + rowNum + "\"" + ">";
                        CreativeTeam += "<th style=\"display:none\">" + id + "</th>";
                        CreativeTeam += "<th> " + userid + "</th>";
                        CreativeTeam += "<th> " + first + "</th>";
                        CreativeTeam += "<th> " + last + "</th>";
                        CreativeTeam += "<th style=\"display:none\"> " + role + "</th>";
                        CreativeTeam += "<th style=\"display:none\"> " + status + "</th>";
                        CreativeTeam += "<th style=\"display: none;\"> " + team + "</th>";
                        if (CheckAdmin(token) || userid == coderID)
                        {
                            CreativeTeam += "<th class=\"edit\"><button class = \"edit\" onclick =\"editCoder(this)\">Edit</button></th>";
                            CreativeTeam += "<th class=\"delete\"><button class = \"delete\" onclick =\"Delete('" + id + "intern');\">Delete</button></th>";
                        }
                    }
                    else if (team == "tech")
                    {
                        TechTeam += "<tr class = \"SELECTME\" value=\"1\" id = \"row " + rowNum + "\"" + ">";
                        TechTeam += "<th style=\"display:none\">" + id + "</th>";
                        TechTeam += "<th> " + userid + "</th>";
                        TechTeam += "<th> " + first + "</th>";
                        TechTeam += "<th> " + last + "</th>";
                        TechTeam += "<th style=\"display:none\"> " + role + "</th>";
                        TechTeam += "<th style=\"display:none\"> " + status + "</th>";
                        TechTeam += "<th style=\"display: none;\"> " + team + "</th>";
                        if (CheckAdmin(token) || userid == coderID)
                        {
                            TechTeam += "<th class=\"edit\"><button class = \"edit\" onclick =\"editCoder(this)\">Edit</button></th>";
                            TechTeam += "<th class=\"delete\"><button class = \"delete\" onclick =\"Delete('" + id + "intern');\">Delete</button></th>";
                        }

                    }
                    else if (team == "operations")
                    {
                        OperationsTeam += "<tr class = \"SELECTME\" value=\"1\" id = \"row " + rowNum + "\"" + ">";
                        OperationsTeam += "<th style=\"display:none\">" + id + "</th>";
                        OperationsTeam += "<th> " + userid + "</th>";
                        OperationsTeam += "<th> " + first + "</th>";
                        OperationsTeam += "<th> " + last + "</th>";
                        OperationsTeam += "<th style=\"display:none\"> " + role + "</th>";
                        OperationsTeam += "<th style=\"display:none\"> " + status + "</th>";
                        OperationsTeam += "<th style=\"display: none;\"> " + team + "</th>";
                        if (CheckAdmin(token) || userid == coderID)
                        {
                            OperationsTeam += "<th class=\"edit\"><button class = \"edit\" onclick =\"editCoder(this)\">Edit</button></th>";
                            OperationsTeam += "<th class=\"delete\"><button class = \"delete\" onclick =\"Delete('" + id + "intern');\">Delete</button></th>";
                        }
                    }
                    DataTeam += "</ tbody ></ table >";
                    CreativeTeam += "</ tbody ></ table >";
                    TechTeam += "</ tbody ></ table >";
                    OperationsTeam += "</ tbody ></ table >";
                    Inner = DataTeam + CreativeTeam + TechTeam + OperationsTeam;
                    rowNum++;
                }
                return Inner;
            }
        }

    }
}
