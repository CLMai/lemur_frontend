﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="LemurID.Index" %>

<!DOCTYPE html>
<html>
<head>
    <!-- Site made with Mobirise Website Builder v4.12.4, https://mobirise.com -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.12.4, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/lemur-logo-no-name-snipping-tool-96x100.jpg" type="image/x-icon">
    <meta name="description" content="">


    <title>Home</title>
    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="preload" as="style" href="assets/mobirise/css/mbr-additional.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
</head>
<body>
    <section class="menu cid-s44dt9LLrN" once="menu" id="menu1-h">



        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <div class="menu-logo">
                <div class="navbar-brand">
                    <span class="navbar-logo">
                        <a href="https://mobirise.co">
                            <img src="assets/images/lemur-logo-no-name-snipping-tool-96x100.jpg" alt="Mobirise" title="" style="height: 3.8rem;">
                        </a>
                    </span>
                    <span class="navbar-caption-wrap"><a class="navbar-caption text-white display-4" href="https://mobirise.co">LemurID</a></span>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                    <li class="nav-item">
                        <a class="nav-link link text-white display-4" href="index.html#form1-x"><span class="mbri-letter mbr-iconfont mbr-iconfont-btn"></span>Contact Us &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link link text-white display-4" href="internlogin.aspx"><span class="mbri-letter mbr-iconfont mbr-iconfont-btn"></span>Login &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</a>
                    </li>
                </ul>

            </div>
        </nav>
    </section>

    <section class="engine"><a href="https://mobirise.info/g">build a site</a></section>
    <section class="cid-qTkA127IK8 mbr-fullscreen" id="header2-1">



        <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(14, 32, 10);"></div>

        <div class="container align-center">
            <div class="row justify-content-md-center">
                <div class="mbr-white col-md-10">
                    <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">
                        <br>
                        <br>
                        <br>
                        LemurID</h1>

                    <p class="mbr-text pb-3 mbr-fonts-style display-5">
                        Protect with tech<br>
                    </p>

                </div>
            </div>
        </div>

    </section>

    <section class="header1 cid-s4mmtY36Os" id="header16-s">





        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-10 align-center">
                    <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">Our Mission</h1>



                </div>
            </div>
        </div>

    </section>

    <section class="services2 cid-s4um2ikwdf" id="services2-11">
        <!---->

        <!---->
        <!--Overlay-->
        <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);">
        </div>
        <!--Container-->
        <div class="container">
            <div class="col-md-12">
                <div class="media-container-row">
                    <div class="mbr-figure" style="width: 70%;">
                        <img src="assets/images/img-5684-gradient2-1384x923.jpg" alt="Mobirise" title="">
                    </div>
                    <div class="align-left aside-content">

                        <div class="mbr-section-text">
                            <p class="mbr-text text1 pt-2 mbr-light mbr-fonts-style display-7">
                                Red-bellied lemurs are classed by the International Union of Conservation of Nature as Vulnerable. The greatest threats to them are rainforest destriction, hunting, and the live animal trade. Habitat destruction has led to declining reproductive rates, contributing to the lemurs' declining population. 
                                <br>
                                <br>
                                LemurID harnesses facial recognition tools &nbsp;to identify red-bellied lemurs throughout Eastern Madagascar, allowing researchers and conservationists to observe and track them with reduced interference.
                            </p>

                        </div>
                        <!--Btn-->

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="header1 cid-s4DYjsfqwd" id="header16-12">





        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-10 align-center">
                    <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">How It Works<br>
                    </h1>



                </div>
            </div>
        </div>

    </section>

    <section class="features11 cid-s4muhx7eK0" id="features11-t">





        <div class="container">
            <div class="col-md-12">
                <div class="media-container-row">
                    <div class="mbr-figure m-auto" style="width: 60%;">
                        <img src="assets/images/img-7063-952x635.jpg" alt="Mobirise" title="">
                    </div>
                    <div class=" align-left aside-content">

                        <div class="mbr-section-text">
                        </div>

                        <div class="block-content">
                            <div class="card p-3 pr-3">
                                <div class="media">
                                    <div class=" align-self-center card-img pb-3">
                                        <span class="mbr-iconfont mbri-responsive" style="color: rgb(8, 18, 6); fill: rgb(8, 18, 6);"></span>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="card-title mbr-fonts-style display-7"><strong>Web and Mobile</strong></h4>
                                    </div>
                                </div>

                                <div class="card-box">
                                    <p class="block-text mbr-fonts-style display-7">By accessing our website, the public can stay informed of &nbsp;our team's accomplishments and work. The LemurID application is available &nbsp;for download on a mobile device, giving research technicians ease of access to identify and study red-bellied lemur individuals. &nbsp;</p>
                                </div>
                            </div>

                            <div class="card p-3 pr-3">
                                <div class="media">
                                    <div class="align-self-center card-img pb-3">
                                        <span class="mbr-iconfont mbri-touch" style="color: rgb(8, 18, 6); fill: rgb(8, 18, 6);"></span>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="card-title mbr-fonts-style display-7"><strong>How It's Done&nbsp;</strong></h4>
                                    </div>
                                </div>

                                <div class="card-box">
                                    <p class="block-text mbr-fonts-style display-7">With the mobile app, the user can take a photo of the lemur subject and submit it for analysis. Using machine learning facial recognition, the app will feed the image through our databases and return the lemur's identity.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="header1 cid-s44bHoclND" id="header16-f">





        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-10 align-center">
                    <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">Research</h1>



                </div>
            </div>
        </div>

    </section>

    <section class="features11 cid-s4mahwQ0hS" id="features11-p">





        <div class="container">
            <div class="col-md-12">
                <div class="media-container-row">
                    <div class="mbr-figure m-auto" style="width: 65%;">
                        <img src="assets/images/img-7031-952x635.jpg" alt="Mobirise" title="">
                    </div>
                    <div class=" align-left aside-content">

                        <div class="mbr-section-text">
                        </div>

                        <div class="block-content">
                            <div class="card p-3 pr-3">
                                <div class="media">

                                    <div class="media-body">
                                    </div>
                                </div>

                                <div class="card-box">
                                    <p class="block-text mbr-fonts-style display-7">
                                        LemurID facial recognition  is being developed in collaboration with the University of Arizona department of Anthropology. The mobile app will be used to help researchers identify, track, and study lemur individuals without tranquilization, which has harmful potential. You can find out more about the research behind this initiative here:
                                        <br>
                                    </p>
                                </div>
                            </div>

                            <div class="card p-3 pr-3">
                                <div class="media">

                                    <div class="media-body">
                                    </div>
                                </div>

                                <div class="card-box">
                                    <p class="block-text mbr-fonts-style display-7"><a href="https://leepanthropology.weebly.com/" class="text-success">https://leepanthropology.weebly.com/</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mbr-section form1 cid-s4tgaJQFT6" id="form1-x">




        <div class="container">
            <div class="row justify-content-center">
                <div class="title col-12 col-lg-8">
                    <h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2">Contact Us</h2>

                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="media-container-column col-lg-8" data-form-type="formoid">
                    <!---Formbuilder Form--->
                    <form action="https://mobirise.com/" method="POST" class="mbr-form form-with-styler">
                        <div class="row">

                            <div hidden="hidden" data-form-alert-danger="" class="alert alert-danger col-12">
                            </div>
                        </div>
                        <div class="dragArea row">
                            <div class="col-md-4  form-group" data-for="name">
                                <label for="name-form1-x" class="form-control-label mbr-fonts-style display-7">Name</label>
                                <input type="text" name="name" data-form-field="Name" required="required" class="form-control display-7" id="name-form1-x">
                            </div>
                            <div class="col-md-4  form-group" data-for="email">
                                <label for="email-form1-x" class="form-control-label mbr-fonts-style display-7">Email</label>
                                <input type="email" name="email" data-form-field="Email" required="required" class="form-control display-7" id="email-form1-x">
                            </div>
                            <div data-for="phone" class="col-md-4  form-group">
                                <label for="phone-form1-x" class="form-control-label mbr-fonts-style display-7">Phone</label>
                                <input type="tel" name="phone" data-form-field="Phone" class="form-control display-7" id="phone-form1-x">
                            </div>
                            <div data-for="message" class="col-md-12 form-group">
                                <label for="message-form1-x" class="form-control-label mbr-fonts-style display-7">Message</label>
                                <textarea name="message" data-form-field="Message" class="form-control display-7" id="message-form1-x"></textarea>
                            </div>
                            <div class="col-md-12 input-group-btn align-center">
                                <button type="submit" class="btn btn-primary btn-form display-4" href="mailto:stecot@arizona.edu">SEND FORM</button>
                            </div>
                        </div>
                    </form>
                    <!---Formbuilder Form--->
                </div>
            </div>
        </div>
    </section>

    <section class="cid-s4tgRRXXAN" id="footer1-y">





        <div class="container">
            <div class="media-container-row content text-white">
                <div class="col-12 col-md-3">
                    <div class="media-wrap">
                        <a href="https://mobirise.co/">
                            <img src="assets/images/lemurid-logo-snipping-tool-194x194.jpg" alt="Mobirise" title="">
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <h5 class="pb-3">Address
                </h5>
                    <p class="mbr-text">
                        &nbsp;McClelland Hall, 1130 E Helen St #218 
                        <br>
                        Tucson, AZ 85721
                    </p>
                </div>
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <h5 class="pb-3">In Collaboration With</h5>
                    <p class="mbr-text">University of Arizona Department of Anthropology</p>
                </div>
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <h5 class="pb-3">&nbsp;</h5>
                    <p class="mbr-text">&nbsp;</p>
                </div>
            </div>
            <div class="footer-lower">
                <div class="media-container-row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>
                <div class="media-container-row mbr-white">
                    <div class="col-sm-6 copyright">
                        <p class="mbr-text mbr-fonts-style display-7">&nbsp;</p>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="assets/web/assets/jquery/jquery.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/tether/tether.min.js"></script>
    <script src="assets/smoothscroll/smooth-scroll.js"></script>
    <script src="assets/dropdown/js/nav-dropdown.js"></script>
    <script src="assets/dropdown/js/navbar-dropdown.js"></script>
    <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
    <script src="assets/theme/js/script.js"></script>


</body>
</html>


